/******************************************************************************
 *
 * The copyright in this software is owned by Rockchip and/or its licensors.
 * This software is made available subject to the conditions of the license
 * terms to be determined and negotiated by Rockchip and you.
 * THIS SOFTWARE IS PROVIDED TO YOU ON AN "AS IS" BASIS and ROCKCHP AND/OR
 * ITS LICENSORS DISCLAIMS ANY AND ALL WARRANTIES AND REPRESENTATIONS WITH
 * RESPECT TO SUCH SOFTWARE, WHETHER EXPRESS,IMPLIED, STATUTORY OR OTHERWISE,
 * INCLUDING WITHOUT LIMITATION, ANY IMPLIED WARRANTIES OF TITLE, NON-INFRINGEMENT,
 * MERCHANTABILITY, SATISFACTROY QUALITY, ACCURACY OR FITNESS FOR A PARTICULAR PURPOSE.
 * Except as expressively authorized by Rockchip and/or its licensors, you may not
 * (a) disclose, distribute, sell, sub-license, or transfer this software to any third party,
 * in whole or part; (b) modify this software, in whole or part; (c) decompile, reverse-engineer,
 * dissemble, or attempt to derive any source code from the software.
 *
 *****************************************************************************/
#include <ebase/types.h>
#include <ebase/trace.h>
#include <ebase/builtins.h>

#include <common/return_codes.h>

#include "isi.h"
#include "isi_iss.h"
#include "isi_priv.h"
#include "RK628D_priv.h"


/*****************************************************************************
 * DEFINES
 *****************************************************************************/


/*****************************************************************************
 * GLOBALS
 *****************************************************************************/

// The settings may be altered by the code in IsiSetupSensor.
const IsiRegDescription_t RK628D_g_aRegDescription[] =
{
	{0x0000 ,0x00,"eTableEnd",eTableEnd}
};

const IsiRegDescription_t RK628D_g_aRegVedioON[] =
{
	{0x0000 ,0x00,"eTableEnd",eTableEnd}
};

const IsiRegDescription_t RK628D_g_hdmi_input_check[] =
{
    {0x0000 ,0x00,"eTableEnd",eTableEnd}
};

const IsiRegDescription_t RK628D_RS1[] =
{
	{0x0000,0x00,"eTableEnd",eTableEnd}
};

const IsiRegDescription_t RK628D_RS2[] =
{
	{0x0000, 0x0000, "eTableEnd", eTableEnd}
};

const IsiRegDescription_t RK628D_RS3_30fps_interlace[] =
{
	{0x0000 ,0x00,"eTableEnd",eTableEnd}
};

const IsiRegDescription_t RK628D_RS3_60fps[] =
{
	{0x0000 ,0x00,"eTableEnd",eTableEnd},
};

const IsiRegDescription_t RK628D_RS5[] =
{
	{0x0000 ,0x00,"eTableEnd",eTableEnd}
};

const IsiRegDescription_t RK628D_RS6[] =
{
	{0x0000 ,0x00,"eTableEnd",eTableEnd}
};

