/******************************************************************************
 *
 * The copyright in this software is owned by Rockchip and/or its licensors.
 * This software is made available subject to the conditions of the license
 * terms to be determined and negotiated by Rockchip and you.
 * THIS SOFTWARE IS PROVIDED TO YOU ON AN "AS IS" BASIS and ROCKCHP AND/OR
 * ITS LICENSORS DISCLAIMS ANY AND ALL WARRANTIES AND REPRESENTATIONS WITH
 * RESPECT TO SUCH SOFTWARE, WHETHER EXPRESS,IMPLIED, STATUTORY OR OTHERWISE,
 * INCLUDING WITHOUT LIMITATION, ANY IMPLIED WARRANTIES OF TITLE, NON-INFRINGEMENT,
 * MERCHANTABILITY, SATISFACTROY QUALITY, ACCURACY OR FITNESS FOR A PARTICULAR PURPOSE.
 * Except as expressively authorized by Rockchip and/or its licensors, you may not
 * (a) disclose, distribute, sell, sub-license, or transfer this software to any third party,
 * in whole or part; (b) modify this software, in whole or part; (c) decompile, reverse-engineer,
 * dissemble, or attempt to derive any source code from the software.
 *
 *****************************************************************************/
#ifndef __RK628D_PRIV_H__
#define __RK628D_PRIV_H__

#include <ebase/types.h>
#include <common/return_codes.h>
#include <hal/hal_api.h>

/*
*v0.1.0x00 : Create file;
*/
#define CONFIG_SENSOR_DRV_VERSION KERNEL_VERSION(0, 1, 0)

#ifdef __cplusplus
extern "C"
{
#endif

#define RK628D_SOFTWARE_RST_REG			(0x7080)
#define RK628D_SOFTWARE_RST_VAL			(0x7080)

#define RK628D_CHIP_ID_HIGH_BYTE_DEFAULT	(0x0147)
#define RK628D_CHIP_ID_MIDDLE_BYTE_DEFAULT	(0x0081)
#define RK628D_CHIP_ID_LOW_BYTE_DEFAULT		(0x0000)

#define RK628D_CHIP_ID_HIGH_BYTE		(0x0000)
#define RK628D_CHIP_ID_MIDDLE_BYTE		(0x0002)
#define RK628D_CHIP_ID_LOW_BYTE			(0x0004)
/*****************************************************************************
* Further defines for driver management
*****************************************************************************/
typedef enum
{
    STATUS_POWER_ON	= 0,
    STATUS_STANDBY	= 1,
    STATUS_READY	= 2,
    STATUS_VIDEO_TX	= 3
} TcStatus;

/*****************************************************************************
 *context structure
 *****************************************************************************/
typedef struct RK628D_Context_s
{
    IsiSensorContext_t  IsiCtx;                 /**< common context of ISI and ISI driver layer; @note: MUST BE FIRST IN DRIVER CONTEXT */

    //// modify below here ////

    IsiSensorConfig_t   Config;                 /**< sensor configuration */
    bool_t              Configured;             /**< flags that config was applied to sensor */
    bool_t              Streaming;              /**< flags that sensor is streaming data */
    bool_t              TestPattern;            /**< flags that sensor is streaming test-pattern */

    bool_t              isAfpsRun;              /**< if true, just do anything required for Afps parameter calculation, but DON'T access SensorHW! */

    bool_t              GroupHold;

    float               VtPixClkFreq;           /**< pixel clock */
    uint16_t            LineLengthPck;          /**< line length with blanking */
    uint16_t            FrameLengthLines;       /**< frame line length */

    float               AecMaxGain;
    float               AecMinGain;
    float               AecMaxIntegrationTime;
    float               AecMinIntegrationTime;

    float               AecIntegrationTimeIncrement; /**< _smallest_ increment the sensor/driver can handle (e.g. used for sliders in the application) */
    float               AecGainIncrement;            /**< _smallest_ increment the sensor/driver can handle (e.g. used for sliders in the application) */

    float               AecCurGain;
    float               AecCurIntegrationTime;

    uint16_t            OldGain;               /**< gain multiplier */
    uint32_t            OldCoarseIntegrationTime;
    uint32_t            OldFineIntegrationTime;

    IsiSensorMipiInfo   IsiSensorMipiInfo;
    bool bHdmiinExit;
    osThread gHdmiinThreadId;
    TcStatus gStatus;
	int res_fd;
	int stream_fd;
} RK628D_Context_t;

#ifdef __cplusplus
}
#endif

#endif

