package teaonly.rk.droidipcam;


import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import teaonly.rk.droidipcam.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("NewApi")
public class FileListViewAdapter extends BaseExpandableListAdapter {

	static public class SuperTreeNode {
		FileInfo parent;
		public List<FileInfo> childs = new ArrayList<FileInfo>();
	}

	private final String TAG = "FileListViewAdapter";
	private final LayoutInflater mInflater;
	
	
	private List<SuperTreeNode> superTreeNodes = new ArrayList<SuperTreeNode>();
	private Context mParentContext;
	
	public FileListViewAdapter(Context context) {
		mParentContext = context;
		mInflater = LayoutInflater.from(context);
	}

	public List<SuperTreeNode> GetTreeNode() {
		return superTreeNodes;
	}

	public void UpdateTreeNode(List<SuperTreeNode> node) {
		superTreeNodes = node;
	}
	
	public void RemoveAll()
	{
		superTreeNodes.clear();
	}
	
	public Object getChild(int groupPosition, int childPosition) {
		return superTreeNodes.get(groupPosition).childs.get(childPosition);
	}

	public int getChildrenCount(int groupPosition) {
		return superTreeNodes.get(groupPosition).childs.size();
	}

	public View getExpandableListView(boolean isgroup,int groupPosition, int childPosition,View convertView) {
		FileInfo info  =  null;
		if(isgroup){
			info = (FileInfo)getGroup(groupPosition);
		}else{
			info = (FileInfo)getChild(groupPosition, childPosition);
		}
		if (convertView == null) {
			//convertView = mInflater.inflate(R.layout.folder_listview_adapter, parent, false);
			convertView = mInflater.inflate(R.layout.normal_adapter, null);
		}   
		//-----------------------------
		final ImageView image = (ImageView) convertView
				.findViewById(R.id.nor_image);
		final TextView text = (TextView) convertView
				.findViewById(R.id.nor_text);
		final TextView text_right = (TextView) convertView
				.findViewById(R.id.nor_right_text);
		final TextView text_choice = (TextView) convertView
				.findViewById(R.id.nor_text_choice);
		image.setImageDrawable(info.icon);
       
		text.setText(info.file.getName());
		String temp_right = null;
		
		if (info.isDir) {
			temp_right = new String("");
		} else {
			 convertView.setPadding(image.getPaddingLeft()+50, image.getPaddingTop(), image.getPaddingRight(), image.getPaddingBottom());
			long temp_size = info.file.length();
			temp_right = new String("");
			temp_right += change_Long_to_String(temp_size);
		}

		temp_right += " | ";
		long lastModified = info.file.lastModified();
		temp_right += change_long_to_time(lastModified);

		temp_right += " | ";
		temp_right += getAttribute(info.file);
		info.isWrite = info.file.canWrite();
		info.isRead = info.file.canRead();

		text_right.setText(temp_right);
		return convertView;
	}

	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		final View treeView = getExpandableListView(false,groupPosition,childPosition,convertView);
		//ExpandableListView treeView = new ExpandableListView(mParentContext);
		//�ؼ�㣺ȡ��ѡ�еĶ������β˵��ĸ��ӽڵ�,���ظ��ⲿ�ص�����
		//treeView.setOnChildClickListener(this.stvClickEvent);
		
		return treeView;
	}

	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		return getExpandableListView(true, groupPosition, -1, convertView);
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	public Object getGroup(int groupPosition) {
		return superTreeNodes.get(groupPosition).parent;
	}

	public int getGroupCount() {
		return superTreeNodes.size();
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	public boolean hasStableIds() {
		return true;
	}
	
	public String change_Long_to_String(long temp_change){
		String temp_str = new String("");
		DecimalFormat df = new DecimalFormat("########.00"); 

		
		if(temp_change >= 1024){	
			float i =  temp_change / 1024f;
			if(i >= 1024f){			
				float j = i / 1024f;
				if(j >= 1024f){		
					float k = j / 1024f;
                                        temp_str += df.format(k);
                                        temp_str += " G";
				}else{
					temp_str += df.format(j);
					temp_str += " M";
				}
			}else{
				temp_str += df.format(i);
				temp_str += " K";
			}
		}else{
			temp_str += temp_change;
			temp_str += " B";
		}
		return temp_str;
	}
	
	public String change_long_to_time(long temp_time){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		String mDateTime1=formatter.format(temp_time); 
		return mDateTime1;
	}
	public String getAttribute(File file){
		String temp_Attr = new String("");
		if(file.isDirectory()){
			temp_Attr += "d";
		}else{
			temp_Attr +="-";
		}
		
		if(file.canRead()){
			temp_Attr += "r";
		}else{
			temp_Attr +="-";
		}
		
		if(file.canWrite()){
			temp_Attr += "w";
		}else{
			temp_Attr +="-";
		}
		
		return temp_Attr;
	}
}
