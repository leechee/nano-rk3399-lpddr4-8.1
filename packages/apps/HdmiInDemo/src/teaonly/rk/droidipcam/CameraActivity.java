
package teaonly.rk.droidipcam;

import java.io.IOException;
import java.util.zip.Inflater;

import teaonly.rk.droidipcam.ICameraCrashService;
import teaonly.rk.droidipcam.R;
import teaonly.rk.droidipcam.CameraManager.CameraOpenErrorCallback;
import teaonly.rk.droidipcam.CameraUtil.RecordState;
import teaonly.rk.droidipcam.CameraUtil.ServiceToken;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.drawable.AnimationDrawable;
import android.net.wifi.WpsInfo;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.provider.Settings;
import android.app.Service; 

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@SuppressLint({
        "NewApi", "ValidFragment"
})
public class CameraActivity extends Activity {
    private static final int MENU_EXIT = 0xCC882201;
    private static final String TAG = "CameraActivity";

    // NativeAgent nativeAgt;
    CameraView myCamView;
    SurfaceView mSurfaceView;
    ImageView mForgeGroundImageView;
    StreamingServer strServer;

    TextView myMessage;
    Button btnStart;
    Button btnBkRecord;
    RadioGroup resRadios;
    //PopupWindow popupMenu;
    LinearLayout recordingstatusLl;
    ImageView recordingView;
    AnimationDrawable animDance;

    boolean inServer = false;
    boolean inStreaming = false;
    static int targetWid = 1920;
    static int targetHei = 1080;

    final String checkingFile = "/mnt/sdcard/ipcam/myvideo.mp4";
    final String resourceDirectory = "/mnt/sdcard/ipcam";
    private ICameraCrashService mService = null;
    private ServiceToken mToken;

    private boolean isStop;
    private static final int DIALOG_CREATE_SETTING_MSG = 0;
    private static final int DIALOG_QUIT_MSG = 1;
    private static final int DIALOG_FILE_MSG = 2;
    private static final int DIALOG_RECORD_MENU =3;
    private static final int DIALOG_PLAY_MENU = 4;
    private static final int DIALOG_SOUND_SELECT = 5;
    DialogFragment newFragment;
    private MyBrocastReceiver receiver;
    private int mAudioOutput = 6;
    private AudioStream mAudioStream;

    // memory object for encoder
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            mAudioOutput = msg.what;
            mAudioStream.switchAudioOutput(mAudioOutput);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate=====");

        mAudioStream = new AudioStream(getApplicationContext());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        win.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.main);
         //注册广播
         registerBrocastReceiver();
    }

    private void registerBrocastReceiver() {
		// TODO Auto-generated method stub
        receiver=new MyBrocastReceiver();
		IntentFilter filter=new IntentFilter();
		filter.addAction("bestom.hdmiin");
		this.registerReceiver(receiver, filter);
	}

	@Override
    public boolean onCreateOptionsMenu(Menu m) {
        m.add(0, MENU_EXIT, 0, "Exit");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem i) {
        switch (i.getItemId()) {
            case MENU_EXIT:
                finish();
                System.exit(0);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroy() {
        Log.d(TAG,"onDestroy=====");
        super.onDestroy();
        this.unregisterReceiver(receiver);
    }

    @Override
    public void onStart() {
        Log.d(TAG,"onStart=====");
        super.onStart();
        mToken = CameraUtil.bindToService(this, osc);
        /*
         * Intent intent2 = new Intent(Intent.ACTION_MAIN, null); intent2.setClass(this,
         * CameraCrashService.class); startService(intent2);
         */
        setup();
       isStop = false;
        
    }
    private void EnableHDMIInAudio(boolean enable)
    {
        if (enable) {
            mAudioStream.start(mAudioOutput);
        } else {
            mAudioStream.stop();
        }
    }

    private ServiceConnection osc = new ServiceConnection() {
        public void onServiceConnected(ComponentName classname, IBinder obj) {
            Log.d(TAG,"onServiceConnected=====");
            mService = ICameraCrashService.Stub.asInterface(obj);
            myCamView.setService(mService);
            //setStartText();
            // startPlayback();
            // doAction();

            // Assume something is playing when the service says it is,
            // but also if the audio ID is valid but the service is paused.
            /*
             * if (mService.getAudioId() >= 0 || mService.isPlaying() || mService.getPath() != null)
             * { // something is playing now, we're done mRepeatButton.setVisibility(View.VISIBLE);
             * mShuffleButton.setVisibility(View.VISIBLE); mQueueButton.setVisibility(View.VISIBLE);
             * setRepeatButtonImage(); setShuffleButtonImage(); setPauseButtonImage(); return; }
             */
             EnableHDMIInAudio(true);
        }

        public void onServiceDisconnected(ComponentName classname) {
            Log.d(TAG,"onServiceDisconnected=====");
            mService = null;
            EnableHDMIInAudio(false);
        }
    };

    public void onStop() {
        Log.d(TAG,"onStop=====");
        //CameraUtil.unbindFromService(mToken);
        super.onStop();
    }

    @Override
    public void onResume() {
        Log.d(TAG,"onResume=====");
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG,"onPause=====");
        CameraUtil.unbindFromService(mToken);
        EnableHDMIInAudio(false);
    }

    private void clearResource() {
        String[] str = {
                "rm", "-r", resourceDirectory
        };

        try {
            Process ps = Runtime.getRuntime().exec(str);
            try {
                ps.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setup() {
        clearResource();

        // NativeAgent.LoadLibraries();
        // nativeAgt = new NativeAgent();
        mForgeGroundImageView = (ImageView) findViewById(R.id.foregroundImageView);
        myCamView = (CameraView) findViewById(R.id.surface_overlay);
        mSurfaceView = (SurfaceView) findViewById(R.id.surface_camera);

        mForgeGroundImageView.bringToFront();

        myCamView.SetupCamera(mSurfaceView, mService, mForgeGroundImageView);

        recordingstatusLl = (LinearLayout) findViewById(R.id.recordingstatus);
        recordingView = (ImageView) findViewById(R.id.recordingImg);
        animDance = (AnimationDrawable) recordingView.getBackground();
        recordingstatusLl.setVisibility(View.INVISIBLE);

        /*btnStart = (Button) findViewById(R.id.btn_start);
        btnStart.setOnClickListener(startAction);
        btnBkRecord = (Button) findViewById(R.id.btn_back_record);
        btnBkRecord.setOnClickListener(startAction);
        findViewById(R.id.btn_setting).setOnClickListener(startAction);
        findViewById(R.id.btn_file).setOnClickListener(startAction);*/
        // btnStart.setEnabled(true);

        
         /*RadioButton rb = (RadioButton) findViewById(R.id.res_low);
         rb.setOnClickListener(low_res_listener); 
         rb = (RadioButton) findViewById(R.id.res_medium); 
         rb.setOnClickListener(medium_res_listener); 
         rb = (RadioButton) findViewById(R.id.res_high); 
         rb.setOnClickListener(high_res_listener);
         resRadios = (RadioGroup) findViewById(R.id.resolution);
         //View v = (View)findViewById(R.id.layout_setup); 
         //v.setVisibility(View.VISIBLE);*/
         
    }

    /*private void setStartText() {
        try {
            int recordState = mService != null ? mService.getRecordState() : RecordState.NONE.value;
            if (recordState != RecordState.NONE.value) {
                btnStart.setText(R.string.action_stop);
            } else {
                btnStart.setText(R.string.action_start);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }*/

    private void doAction(final boolean isBack) {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Toast.makeText(getApplicationContext(),
                    R.string.no_storage, Toast.LENGTH_LONG).show();
            return;
        }
        if (mService == null) {
            showToast(CameraActivity.this,
                    getString(R.string.msg_prepare_error1));
            return;
        }
        if (isBack){
            /*Intent pipIntent = new Intent(PIPBroadCastReceiver.ACTION_MOVIE_START);
            pipIntent.putExtra("width", 1000);
            pipIntent.putExtra("height", 500);
            sendBroadcast(pipIntent);
            finish();
            }*/
            try {
            mService.pip(400,300);
            finish();
            //Intent intent = new Intent(CameraActivity.this, FloatWindowActivity.class);
            //startActivity(intent);
        } catch (RemoteException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
            
        }
        else{
            boolean ret = false;
            try {
                mService.prepareMedia();
                ret = mService.startRecording();
                if(!ret) {
                    showToast(CameraActivity.this,
                            getString(R.string.msg_prepare_error1));
                }
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
           
        }
       /* mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    boolean ret = false;
                    mService.prepareMedia();
                    ret = mService.startRecording();
                    if (ret) {
                        if(isBack)
                            finish();
                    } else {
                        showToast(CameraActivity.this,
                                getString(R.string.msg_prepare_error1));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 100);*/
        

        /*mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                setStartText();
            }
        }, 300);*/
    }

    private void showToast(Context context, String message) {
        // create the view
        LayoutInflater vi = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = vi.inflate(R.layout.message_toast, null);

        // set the text in the view
        TextView tv = (TextView) view.findViewById(R.id.message);
        tv.setText(message);

        // show the toast
        Toast toast = new Toast(context);
        toast.setView(view);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String cmd = intent.getStringExtra("command");
            Log.d(TAG, "mIntentReceiver.onReceive " + action + " / " + cmd);
            if (CameraUtil.START_PREVIEW.equals(action)) {
            }
        }
    };
    private AlertDialog dialog;


    void showDialog() {
       newFragment = MyAlertDialogFragment.newInstance();
        newFragment.show(getFragmentManager(), "dialog");
       
    }

    public static class MyAlertDialogFragment extends DialogFragment {


        public static MyAlertDialogFragment newInstance() {
            MyAlertDialogFragment frag = new MyAlertDialogFragment();
            Bundle args = new Bundle();
            // args.putInt("title", title);
            frag.setArguments(args);
            return frag;
        }

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            // Get the layout inflater
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View setupView = inflater.inflate(R.layout.set_up_dialog, null);
            /*
             * RadioButton rb = (RadioButton)setupView. findViewById(R.id.res_low);
             * rb.setOnClickListener(low_res_listener); rb = (RadioButton)setupView.
             * findViewById(R.id.res_medium); rb.setOnClickListener(medium_res_listener); rb =
             * (RadioButton) setupView.findViewById(R.id.res_high);
             * rb.setOnClickListener(high_res_listener);
             */
            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            builder.setView(setupView)
                    // Add action buttons
                    .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // sign in the user ...
                        }
                    })
                    .setNegativeButton(R.string.dialog_cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            return  builder.create();
        }
    }

    // END_INCLUDE(dialog)
    @Override
    public Dialog onCreateDialog(int dialogId) {
        switch (dialogId) {
            case DIALOG_CREATE_SETTING_MSG:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                // Get the layout inflater
                LayoutInflater inflater = getLayoutInflater();
                View setupView = inflater.inflate(R.layout.set_up_dialog, null);
                RadioButton rb = (RadioButton) setupView.findViewById(R.id.res_low);
                rb.setOnClickListener(low_res_listener);
                rb = (RadioButton) setupView.findViewById(R.id.res_medium);
                rb.setOnClickListener(medium_res_listener);
                rb = (RadioButton) setupView.findViewById(R.id.res_high);
                rb.setOnClickListener(high_res_listener);
                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                builder.setView(setupView)
                        // Add action buttons
                        .setPositiveButton(R.string.dialog_ok,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        // sign in the user ...
                                    }
                                })
                        .setNegativeButton(R.string.dialog_cancel,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                return dialog = builder.create();
            case DIALOG_QUIT_MSG:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setCancelable(true);
                // builder2.setTitle();
                builder2.setItems(R.array.back_dialog, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface paramDialogInterface, int witch)
                    {
                        if (witch == 0) {
                            Log.i("MainActivity", "------------DIALOG_QUIT_MSG");
                            dismissDialog(DIALOG_QUIT_MSG);
                            showDialog(DIALOG_SOUND_SELECT);
                        } else {
                            try {
                                mService.stopRecording();
                                // stopService(name);
                            } catch (RemoteException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            EnableHDMIInAudio(false);
                            finish();
                        }
                    }
                });
                return dialog = builder2.create();
            case DIALOG_PLAY_MENU:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                View playMenuView = getLayoutInflater().inflate(R.layout.play_menu, null);
                LinearLayout pipButton = (LinearLayout) playMenuView.findViewById(R.id.linear_pip);
                LinearLayout exitButton = (LinearLayout) playMenuView.findViewById(R.id.linear_exit);
                LinearLayout recordButton = (LinearLayout) playMenuView.findViewById(R.id.linear_record);

                pipButton.setSelected(true);
                pipButton.setOnClickListener(new OnClickListener() {
                    
                    @Override
                    public void onClick(View v) {
                        doAction(true);
                        dismissDialog(DIALOG_PLAY_MENU);
                    }
                });
                exitButton.setOnClickListener(exit_listener);
                recordButton.setOnClickListener(new OnClickListener() {
                    
                    @Override
                    public void onClick(View v) {
                        recordingstatusLl.setVisibility(View.VISIBLE);
                        animDance.start(); 
                        record();
                        dismissDialog(DIALOG_PLAY_MENU);
                    }
                });
                builder3.setView(playMenuView);
                return  dialog = builder3.create();
            case DIALOG_RECORD_MENU:
                AlertDialog.Builder builder4 = new AlertDialog.Builder(this);
                View recordingMenuView = getLayoutInflater().inflate(R.layout.recording_menu, null);
                LinearLayout exitButton1 = (LinearLayout) recordingMenuView
                        .findViewById(R.id.linear_exit1);
                LinearLayout stopRecordingButton = (LinearLayout) recordingMenuView
                        .findViewById(R.id.linear_stoprecord);

                stopRecordingButton.setSelected(true);
                stopRecordingButton.setOnClickListener(new OnClickListener() {
                    
                    @Override
                    public void onClick(View v) {
                        animDance.stop(); //
                        recordingstatusLl.setVisibility(View.INVISIBLE);
                        stopRecord();
                        dismissDialog(DIALOG_RECORD_MENU);
                        
                    }
                });
                exitButton1.setOnClickListener(exit_listener);
                builder4.setView(recordingMenuView);
                return builder4.create();
            case DIALOG_SOUND_SELECT:
                AlertDialog.Builder builder5 = new AlertDialog.Builder(this);
                builder5.setCancelable(true);
                builder5.setSingleChoiceItems(R.array.sound_select, 6,
                new DialogInterface.OnClickListener() {
                    public void onClick(
                    DialogInterface paramDialogInterface, int witch) {
                        mHandler.sendEmptyMessage(witch);
                        dismissDialog(DIALOG_SOUND_SELECT);
                    }
                });
                return dialog = builder5.create();
        }
        return super.onCreateDialog(dialogId);
    }

    private OnClickListener low_res_listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            targetWid = 320;
            targetHei = 240;
        }
    };
    private OnClickListener medium_res_listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            targetWid = 640;
            targetHei = 480;
        }
    };
    private OnClickListener high_res_listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            targetWid = 1280;
            targetHei = 720;
        }
    };

    
    
    private OnClickListener pip_listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            doAction(true);
            //dismissDialog(DIALOG_RECORD_MENU);
        }
    };
    
    private OnClickListener stopRecord_listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            animDance.stop(); //
            recordingstatusLl.setVisibility(View.INVISIBLE);
            stopRecord();
        }
    };
    private OnClickListener exit_listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                mService.stopRecording();
                // stopService(name);
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            EnableHDMIInAudio(false);
            CameraActivity.this.finish();
            
        }
    };
    private OnClickListener recording_listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            recordingstatusLl.setVisibility(View.VISIBLE);
            animDance.start(); 
            record();
        }
    };

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        /*
         * if (relLay.getParent() != null) { WindowManager wm = (WindowManager)
         * getApplicationContext() .getSystemService("window"); wm.removeView(relLay); // back(); }
         */
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showDialog(DIALOG_QUIT_MSG);
        }
        if(keyCode == KeyEvent.KEYCODE_MENU){
            if (!animDance.isRunning()) {
                Log.i(TAG, "-----------DIALOG_PLAY_MENU------"+animDance.isRunning());
                showDialog(DIALOG_PLAY_MENU);
            } else {
                Log.i(TAG, "-----------DIALOG_RECORD_MENU------"+animDance.isRunning());
                showDialog(DIALOG_RECORD_MENU);
                
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showDialog(DIALOG_QUIT_MSG);
        }
        return true;
    }

    /*private OnClickListener startAction = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.btn_start) {
                int recordState = 0;
                try {
                    recordState = mService != null ? mService.getRecordState() : 0;
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                if (recordState != RecordState.NONE.value) {
                    try {
                        mService.stopRecording();
                        // stopService(name);
                    } catch (RemoteException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    setStartText();
                } else {
                    doAction(false);
                }
            } else if (v.getId() == R.id.btn_back_record) {
                doAction(true);
            } else if (v.getId() == R.id.btn_setting) {
                int recordState = 0;
                try {
                    recordState = mService != null ? mService.getRecordState() : 0;
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                if (recordState == RecordState.NONE.value) {
                    showDialog(DIALOG_CREATE_SETTING_MSG);
                } else {
                    showToast(CameraActivity.this,
                            getString(R.string.stop_record));
                    // Toast.makeText(getApplicationContext(), R.string.stop_record,
                    // Toast.LENGTH_LONG).show();
                }

            } else if (v.getId() == R.id.btn_file) {
                Log.i(TAG, "-----------------"+animDance.isRunning());
                if (!animDance.isRunning()) {
                    Log.i(TAG, "-----------DIALOG_PLAY_MENU------"+animDance.isRunning());
                    showDialog(DIALOG_PLAY_MENU);
                } else {
                    Log.i(TAG, "-----------DIALOG_RECORD_MENU------"+animDance.isRunning());
                    showDialog(DIALOG_RECORD_MENU);
                    
                }

                // doAction(true);
            }
        }
    };*/

    public CameraOpenErrorCallback getCameraOpenErrorCallback() {
        return mCameraOpenErrorCallback;
    }

    private CameraOpenErrorCallback mCameraOpenErrorCallback = new CameraOpenErrorCallback() {
        @Override
        public void onCameraDisabled(int cameraId) {

            CameraUtil.showErrorAndFinish(CameraActivity.this,
                    R.string.camera_disabled);

        }

        @Override
        public void onDeviceOpenFailure(int cameraId) {

            CameraUtil.showErrorAndFinish(CameraActivity.this,
                    R.string.cannot_connect_camera);

        }

        @Override
        public void onReconnectionFailure(CameraManager mgr) {

            CameraUtil.showErrorAndFinish(CameraActivity.this,
                    R.string.cannot_connect_camera);

        }
    };

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // TODO Auto-generated method stub
        if (KeyEvent.KEYCODE_MENU == event.getKeyCode()) {

        }
        return super.dispatchKeyEvent(event);
    }

    private View initPlayMenu() {
        View playMenuView = getLayoutInflater().inflate(R.layout.play_menu, null);
        LinearLayout pipButton = (LinearLayout) playMenuView.findViewById(R.id.linear_pip);
        LinearLayout exitButton = (LinearLayout) playMenuView.findViewById(R.id.linear_exit);
        LinearLayout recordButton = (LinearLayout) playMenuView.findViewById(R.id.linear_record);

        pipButton.setSelected(true);
        pipButton.setOnClickListener(pip_listener);
        exitButton.setOnClickListener(exit_listener);
        recordButton.setOnClickListener(recording_listener);
        return playMenuView;
    }

    private View initRecordingMenu() {
        View recordingMenuView = getLayoutInflater().inflate(R.layout.recording_menu, null);
        LinearLayout exitButton = (LinearLayout) recordingMenuView
                .findViewById(R.id.linear_exit1);
        LinearLayout stopRecordingButton = (LinearLayout) recordingMenuView
                .findViewById(R.id.linear_stoprecord);

        stopRecordingButton.setSelected(true);
        stopRecordingButton.setOnClickListener(stopRecord_listener);
        exitButton.setOnClickListener(exit_listener);
        return recordingMenuView;
    }

    private View getMenu(boolean type) {

        if (type) {
            return initRecordingMenu();
        }
        else {
            return initPlayMenu();
        }

    }

    private void stopRecord() {
        int recordState = 0;
        try {
            recordState = mService != null ? mService.getRecordState() : 0;
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if (recordState != RecordState.NONE.value) {
            try {
                mService.stopRecording();
                // stopService(name);
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /**
     * record video
     */
    private void record() {
        int recordState = 0;
        try {
            recordState = mService != null ? mService.getRecordState() : 0;
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if (recordState == RecordState.NONE.value
                || recordState == RecordState.REPEAT_RECORD.value) {
            doAction(false);
        }
    }
    
    class  MyBrocastReceiver extends  BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action=intent.getAction();
			if(action.equals("bestom.hdmiin")){
				try{
				if(mService!=null){
					
				     mService.stopRecording();
				     finish();
				}
				
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
    	
    	
    	
    }
}
